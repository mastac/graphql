<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
  protected $table = 'role';

  public function films()
  {
      return $this->belongsToMany('App\Film', 'actor_film_role', 'role_id', 'film_id')->withPivot('actor_id');
  }

  public function actors()
  {
      return $this->belongsToMany('App\Actor', 'actor_film_role', 'role_id', 'actor_id')->withPivot('film_id');
  }

}
