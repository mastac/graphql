<?php

namespace App\GraphQL\Query;

use Folklore\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use GraphQL;
use App\Film;

class FilmsQuery extends Query
{
    protected $attributes = [
        'name' => 'FilmsQuery',
        'description' => 'A Film query'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('Film'));
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::int()],
            'name' => ['name' => 'name', 'type' => Type::string()],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {
        if (isset($args['id'])) {
            return Film::where('id', $args['id'])->get();
        } 

        if (isset($args['name'])) {
            return Film::where('original_name', 'like', '%'.$args['name'].'%')
            ->orWhere('translate_name', 'like', '%'.$args['name'].'%')->get();
        }
    
        return Film::all();     
    }
}
