<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	Schema::create( 'actor', function ( Blueprint $table ) {
	    $table->increments('id');
      $table->integer('external_actor_id');
      $table->string('name')->nullable();
      $table->string('name_eng')->nullable();
      $table->text('bio')->nullable();
      $table->date('birth_day')->nullable();
      $table->date('death_day')->nullable();
      $table->datetime('update_date')->nullable();
	} );

	Schema::create( 'film', function ( Blueprint $table ) {
	    $table->increments('id');
      $table->string('original_name')->nullable();
      $table->string('translate_name')->nullable();
      $table->integer('year')->nullable();
      $table->integer('external_film_id')->nullable();
      $table->text('description')->nullable();
      $table->datetime('update_date')->nullable();
	} );

  Schema::create( 'genre', function ( Blueprint $table ) {
	    $table->increments('id')->unsigned();
	    $table->string('name');
	} );

  Schema::create( 'film_genre', function ( Blueprint $table ) {
	    $table->integer( 'genre_id' )->unsigned();
	    $table->integer( 'film_id' )->unsigned();

      $table->foreign('genre_id')->references('id')->on('genre');
      $table->foreign('film_id')->references('id')->on('film');

      $table->primary(['genre_id', 'film_id']);
	} );

	Schema::create( 'role', function ( Blueprint $table ) {
	    $table->increments('id');
	    $table->string('name');
	} );

	Schema::create( 'actor_film_role', function ( Blueprint $table ) {
	    $table->integer( 'actor_id' )->unsigned();
	    $table->integer( 'film_id' )->unsigned();
	    $table->integer( 'role_id' )->unsigned();

      $table->foreign('actor_id')->references('id')->on('actor');
      $table->foreign('film_id')->references('id')->on('film');
      $table->foreign('role_id')->references('id')->on('role');

      $table->primary(['actor_id', 'film_id', 'role_id']);
	} );

  Schema::create( 'foto_film', function ( Blueprint $table ) {
	    $table->increments('id')->unsigned();
	    $table->string('foto_url');
      $table->enum('type', ['oboi', 'image', 'main']);
      $table->integer( 'film_id' )->unsigned();

      $table->foreign('film_id')->references('id')->on('film');
	} );

    Schema::create( 'foto_actor', function ( Blueprint $table ) {
  	    $table->increments('id')->unsigned();
  	    $table->string('foto_url');
        $table->enum('type', ['oboi', 'image', 'main']);
        $table->integer( 'actor_id' )->unsigned();

        $table->foreign('actor_id')->references('id')->on('actor');
  	} );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	Schema::drop( 'actor' );
	Schema::drop( 'film' );
	Schema::drop( 'role' );
  Schema::drop( 'genre' );
  Schema::drop( 'film_genre' );
  Schema::drop( 'role' );
	Schema::drop( 'actor_film_role' );
  Schema::drop( 'foto_film' );
  Schema::drop( 'foto_actor' );
    }
}
