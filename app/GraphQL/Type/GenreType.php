<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as BaseType;
use GraphQL;

class GenreType extends BaseType
{
    protected $attributes = [
        'name' => 'GenreType',
        'description' => 'A type genres of film'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the genre'
            ],
            'name' => [
                'type' => Type::string(),
                'description' => 'The name of genre'
            ],
            'films' => [
                'type' => Type::listOf(GraphQL::type('Film')),
                'description' => 'The films of genre'
            ],            
        ];
    }
}
