<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as BaseType;
use GraphQL;

class RoleType extends BaseType
{
    protected $attributes = [
        'name' => 'Role',
        'description' => 'A type role of actor'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the role'
            ],
            'name' => [
                'type' => Type::string(),
                'description' => 'The name of role'
            ],
        ];
    }
}
