import React, { Component } from 'react';

class ActorsFotoPage extends Component {
    render() {
        return (
            <div className="ActorsFotoPage">

                <div className="container">

                    <div className="row">

                        <div className="col-md-1">
                            <div className="card mb-4 shadow-sm">
                                <img src="https://dummyimage.com/70x85/000/fff.png" className="card-img-top" alt="..." />
                                {/*<div className="card-body">*/}
                                    {/*<h5 className="card-title">Card title</h5>*/}
                                    {/*<p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>*/}
                                    {/*<a href="#" className="btn btn-primary">Go somewhere</a>*/}
                                {/*</div>*/}
                            </div>
                        </div>

                    </div>

                </div>

            </div>
        );
    }
}

export default ActorsFotoPage;