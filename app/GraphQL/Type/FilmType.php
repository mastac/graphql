<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as BaseType;
use GraphQL;
use GraphQL\Type\Definition\ResolveInfo;
use App\Film;

class FilmType extends BaseType
{
    protected $attributes = [
        'name' => 'Film',
        'description' => 'A type of film'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the user'
            ],
            'original_name' => [
                'type' => Type::string(),
                'description' => 'The original name of film'
            ],
            'translate_name' => [
                'type' => Type::string(),
                'description' => 'The russian name of film'
            ],
            'year' => [
                'type' => Type::int(),
                'description' => 'The year of film'
            ],
            'external_film_id' => [
                'type' => Type::int(),
                'description' => 'The external id of film'
            ],
            'description' => [
                'type' => Type::string(),
                'description' => 'The description of film'
            ],
            'update_date' => [
                'type' => Type::string(),
                'description' => 'The update_date of film in external site'
            ],
            'actors' => [
                'type' => Type::listOf(GraphQL::type('Actor')),
                'description' => 'The films of film'
            ],
            'fotos' => [
                'type' => Type::listOf(GraphQL::type('FilmFoto')),
                'description' => 'The fotos of film'
            ],
            'genres' => [
                'type' => Type::listOf(GraphQL::type('Genre')),
                'description' => 'The genres of film'
            ],
            'roles' => [
                'type' => Type::listOf(GraphQL::type('Role')),
                'description' => 'The roles of film'
            ],
        ];
    }

    public function resolveRolesField($root, $args, $context, ResolveInfo $info)
    {
        if (isset($root->pivot)) {
            return Film::find($root->id)->roles()->wherePivot('actor_id', $root->pivot->actor_id)->get();
        }

        return $root->roles;
    }
}
