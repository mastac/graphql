<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as BaseType;
use GraphQL;
use App\GraphQL\Type\Enum\FotoEnumType;

class FilmFotoType extends BaseType
{
    protected $attributes = [
        'name' => 'FilmFoto',
        'description' => 'A type foto of film'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the film foto'
            ],
            'foto_url' => [
                'type' => Type::string(),
                'description' => 'The foto url of film foto'
            ],
            'type' => [
                'type' => FotoEnumType::type(),
                'description' => 'The type of foto'
            ],
            'film_id' => [
                'type' => Type::int(),
                'description' => 'The film id of external id'
            ],
        ];
    }
}
