import React, { Component } from 'react';

import { Redirect } from 'react-router-dom';

import './SearchBar.css'

class SearchBar extends Component {

    state = {    
        searchText: '',
        flagSearchClick: false
    }
    
    handleClick = () => {
        this.setState({flagSearchClick: true});
    }    

    inputChangedHandler = (event) => {
        this.setState({searchText: event.target.value});
    }

    render = () => {

        const searchText = this.state.searchText;
        const flagSearchClick = this.state.flagSearchClick;
        if (flagSearchClick) return <Redirect to={{
            pathname: '/search',
            search: '?text=' + searchText
        }} />;        

        return (
            <div className="container">
                <h1 className="big-logo mt-4"><strong>K</strong>ino<strong>S</strong>earch</h1>
                <div className="SearchBar mt-4">
                    <div className="input-group input-group-lg">
                        <input type="text" className="form-control" 
                            value={this.state.searchText} 
                            onChange={(event)=>this.inputChangedHandler(event)}/>
                        <div className="input-group-append">
                            <button className="btn btn-outline-secondary" type="button" id="button-addon2" onClick={this.handleClick}>Поиск</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default SearchBar;