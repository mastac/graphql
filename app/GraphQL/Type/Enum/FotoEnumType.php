<?php
namespace App\GraphQL\Type\Enum;

use GraphQL\Type\Definition\EnumType;

class FotoEnumType
{

    static protected $_type = null;

    static public function type()
    {
        if (!self::$_type) {
            self::$_type = new EnumType([
                'name' => 'type',
                'description' => 'The type of fot image',
                'values' => [
                    'main' => [
                        'value' => 'main',
                        'description' => 'The main foto'
                    ],
                    'image' => [
                        'value' => 'image',
                        'description' => 'The image of foto'
                    ],
                    'oboi' => [
                        'value' => 'oboi',
                        'description' => 'The image of oboi'
                    ],
                ],
            ]);
        }

        return self::$_type;
    }
}