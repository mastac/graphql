<?php

namespace App\GraphQL\Query;

use Folklore\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use GraphQL;
use App\Actor;

class ActorQuery extends Query
{
    protected $attributes = [
        'name' => 'ActorQuery',
        'description' => 'A Actor query'
    ];

    public function type()
    {
        return GraphQL::type('Actor');
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::int()],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {
        if (isset($args['id'])) {
            return Actor::find($args['id']);
        } else {
            return Actor::all();
        }
    }
}
