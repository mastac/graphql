<?php

namespace App\GraphQL\Query;

use Folklore\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use GraphQL;
use App\Genre;

class GenreQuery extends Query
{
    protected $attributes = [
        'name' => 'GenreQuery',
        'description' => 'A Genre query'
    ];

    public function type()
    {
        return GraphQL::type('Genre');
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::int()],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {
        return Genre::find($args['id']);
    }
}
