<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    protected $table = 'genre';

    public function films()
    {
        return $this->belongsToMany('App\Film', 'film_genre', 'genre_id', 'film_id');
//        return $this->belongsTo('App\Film');
    }
}
