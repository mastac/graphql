<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Actor extends Model
{
  protected $table = 'actor';

  public function films()
  {
      return $this->belongsToMany('App\Film', 'actor_film_role', 'actor_id', 'film_id')->withPivot('role_id');
      // return $this->belongsToMany('Drink', 'customer_drinks', 'customer_id', 'drink_id')
      //             ->withPivot('customer_got_drink', 'chair_id');
      //             ->join('chair', 'chair_id', 'chair.id');
      //             ->select('drink_id', 'customer_id', 'pivot_customer_got_drink', 'chair.name AS pivot_chair_name'); //this select is optional

      // return $this->belongsToMany('User','store_user_permissions','store_id','user_id')
  		// 	->withPivot('permission_id')
  		// 	->join('permissions','permission_id','=','permissions.id')
  		// 	->select('permissions.name as pivot_permission_name');
  }

  public function roles()
  {
      return $this->belongsToMany('App\Role', 'actor_film_role', 'actor_id', 'role_id')->withPivot('film_id');
  }

  public function fotos()
  {
      return $this->hasMany('App\ActorFoto', 'actor_id', 'id');
  }

}
