<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FilmFoto extends Model
{
  
  protected $table = 'foto_film';

  public function film()
  {
      return $this->belongsTo('App\Film', 'film_id', 'id');
  }

}
