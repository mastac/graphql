import React, { Component } from 'react';
import { BrowserRouter as Router, Route, NavLink } from "react-router-dom";

class NavigationBar extends Component {
    render() {
        return (
            <div className="NavigationBar">
                <div className="container">
                    <ul className="nav nav-pills my-4">
                        <li className="nav-item">
                            <NavLink to="/actors/list" className="nav-link" activeClassName="active">Актеры</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink to="/films" className="nav-link" activeClassName="active">Фильмы</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink to="/genres" className="nav-link" activeClassName="active">Жанры</NavLink>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}

export default NavigationBar;