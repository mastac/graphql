<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
  protected $table = 'film';

  public function roles()
  {
      return $this->belongsToMany('App\Role', 'actor_film_role', 'film_id', 'role_id')->withPivot('actor_id');
  }

  public function actors()
  {
      return $this->belongsToMany('App\Actor', 'actor_film_role', 'film_id', 'actor_id')->withPivot('role_id');
  }

  public function genres()
  {
      return $this->belongsToMany('App\Genre', 'film_genre', 'film_id', 'genre_id');
  }

  public function fotos()
  {
      return $this->hasMany('App\FilmFoto', 'film_id', 'id');
  }
}
