<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as BaseType;
use GraphQL;
use App\GraphQL\Type\Enum\FotoEnumType;

class ActorFotoType extends BaseType
{
    protected $attributes = [
        'name' => 'ActorFotoType',
        'description' => 'A type actor foto'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the actor foto'
            ],
            'foto_url' => [
                'type' => Type::string(),
                'description' => 'The foto url of actor foto'
            ],
            'type' => [
                'type' => FotoEnumType::type(),
                'description' => 'The type of foto'
            ],
            'actor_id' => [
                'type' => Type::int(),
                'description' => 'The actor id of external id'
            ],
        ];
    }
}
