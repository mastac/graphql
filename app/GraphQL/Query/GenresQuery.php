<?php

namespace App\GraphQL\Query;

use Folklore\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use GraphQL;
use App\Genre;

class GenresQuery extends Query
{
    protected $attributes = [
        'name' => 'GenreQuery',
        'description' => 'A Genre query'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('Genre'));
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::int()],
            'name' => ['name' => 'name', 'type' => Type::string()],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {
        if (isset($args['id'])) {
            return Genre::where('id', $args['id'])->get();
        } 

        if (isset($args['name'])) {
            return Genre::where('name', 'like', '%'.$args['name'].'%')->get();
        }
    
        return Genre::all();                
    }
}
