<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as BaseType;
use GraphQL;

class ActorType extends BaseType
{
    protected $attributes = [
        'name' => 'Actor',
        'description' => 'A type of actor'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the user'
            ],
            'external_actor_id' => [
                'type' => Type::int(),
                'description' => 'The external id of actor'
            ],
            'name' => [
                'type' => Type::string(),
                'description' => 'The name of actor'
            ],
            'name_eng' => [
                'type' => Type::string(),
                'description' => 'The english name of actor'
            ],
            'bio' => [
                'type' => Type::string(),
                'description' => 'The biographi description of actor'
            ],
            'birth_day' => [
                'type' => Type::string(),
                'description' => 'The birth_day of actor'
            ],
            'death_day' => [
                'type' => Type::string(),
                'description' => 'The death_day of actor'
            ],
            'update_date' => [
                'type' => Type::string(),
                'description' => 'The update_date of actor in external site'
            ],
            'films' => [
                'type' => Type::listOf(GraphQL::type('Film')),
                'description' => 'The films of actor'
            ],
            'fotos' => [
                'type' => Type::listOf(GraphQL::type('ActorFoto')),
                'description' => 'The fotos of actor'
            ],
//            'roles' => [
//                'type' => Type::listOf(GraphQL::type('Role')),
//                'description' => 'The roles of actor'
//            ],
            'genres' => [
                'type' => Type::listOf(GraphQL::type('Genre')),
                'description' => 'The genres of actor'
            ],
        ];
    }
}
