<?php

namespace App\GraphQL\Query;

use Folklore\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use GraphQL;
use App\Role;

class RolesQuery extends Query
{
    protected $attributes = [
        'name' => 'RolesQuery',
        'description' => 'A Role query'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('Role'));
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::int()],
            'name' => ['name' => 'name', 'type' => Type::string()],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {
        

        if (isset($args['id'])) {
            return Role::where('id', $args['id'])->get();
        }         

        if (isset($args['name'])) {
            return Role::where('name', 'like', '%'.$args['name'].'%')->get();
        } 
        
        return Role::all();        
    }
}
