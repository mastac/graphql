import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import SearchBar from '../components/SearchBar';

class SearchMainPage extends Component {
    render() {
        return (
            <React.Fragment>
                <SearchBar/>
            </React.Fragment>
        );
    }

}

export default SearchMainPage;