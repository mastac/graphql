import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import './App.css';

import SearchMainPage from './pages/SearchMainPage';
import SearchResultsPage from './pages/SearchResultsPage';
import FilmDetailsPage from './pages/FilmDetailsPage';
import ActorDetailsPage from './pages/ActorDetailsPage';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <div className="container">
              <Switch>
                <Route path='/film' component={FilmDetailsPage}/>
                <Route path='/actor' component={ActorDetailsPage}/>
                <Route path='/search' component={SearchResultsPage}/>
                <Route path='/' component={SearchMainPage}/>
                <Route component={SearchMainPage}/>
              </Switch>
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
