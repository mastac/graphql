<?php

namespace App\GraphQL\Query;

use Folklore\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use GraphQL;
use App\Film;

class FilmQuery extends Query
{
    protected $attributes = [
        'name' => 'FilmQuery',
        'description' => 'A Film query'
    ];

    public function type()
    {
        return GraphQL::type('Film');
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::int()],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {        
        return Film::find($args['id']);
    }
}
