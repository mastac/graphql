import React, { Component } from 'react';

class ActorsListPage extends Component {
    render() {
        return (
            <div className="ActorsListPage">

                <div className="container">

                    <table className="table table-sm">
                        <thead>
                        <tr>
                            <th scope="col">Год</th>
                            <th scope="col">Имя</th>
                            <th scope="col">Фото</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">1</th>
                            <td>Mark</td>
                            <td>+</td>
                        </tr>
                        <tr>
                            <th scope="row">2</th>
                            <td>Jacob</td>
                            <td>-</td>
                        </tr>
                        </tbody>
                    </table>

                </div>

            </div>
        );
    }
}

export default ActorsListPage;