<?php

use Illuminate\Database\Seeder;

class DemoDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     /**
      * Run the database seeds.
      *
      * @return void
      */
     public function run()
     {
         // =============================================================
         // file Path -> Project/app/configs/database.php
         // get the database name, database username, database password
         // =============================================================
         $db     = \Config::get('database.connections.mysql.database');
         $user   = \Config::get('database.connections.mysql.username');
         $pass   = \Config::get('database.connections.mysql.password');
         $host   = \Config::get('database.connections.mysql.host');

         // running command line import in php code
         exec("mysql -h " . $host. " -u " . $user . " --password=\"" . $pass . "\" " . $db . " < ./database/demo-data.sql");
     }
}
