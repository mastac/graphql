import React, { Component } from 'react';

import SearchBar from './SearchBar';
import NavigationBar from './NavigationBar';
import ActorsFotoPage from './ActorsFotoPage';
import ActorsListPage from './ActorsListPage';

import { Route, Switch } from 'react-router-dom'

class Main extends Component {
    render() {
        return (
            <div className="container">
                <SearchBar />
                <NavigationBar />
                <Switch>
                    <Route exact path='/' component={ActorsFotoPage}/>
                    <Route path='/actors/list' component={ActorsListPage}/>
                </Switch>
            </div>
        );
    }

}

export default Main;