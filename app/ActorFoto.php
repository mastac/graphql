<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActorFoto extends Model
{
    protected $table = 'foto_actor';

    public function actor()
    {
        return $this->belongsTo('App\Actor', 'actor_id', 'id');
    }
}
